package net.adamslo.monetukol.Cache;

/**
 * cache record. only structual class
 */
public class Record {
    private String id;
    private String record;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRecord() {
        return record;
    }

    public void setRecord(String record) {
        this.record = record;
    }

}
