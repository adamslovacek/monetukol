package net.adamslo.monetukol.Cache;

import net.adamslo.monetukol.api.CalculatorOperations;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

/**
 * wrapper for ehcache. Not implementing separate class for records. Ilustration of enclosing into the classes are presented elsewhere
 */
public class EhCache {
    /**
     * cacheid
     */
    private final String PATH = "cacheMonet";

    /**
     * Writes stringto chache
     * @param a operant
     * @param b operant
     * @param request requst
     * @param resultString result of the operation
     * @param id identifier
     * @param operation operator
     */
    public void write(String a, String b, HttpServletRequest request, String resultString, long id, CalculatorOperations operation) {
        CacheManager cm = CacheManager.getInstance();
        Cache cache = cm.getCache(PATH);
        cache.put(new Element(id, this.getCacheString(a, b, request, resultString, operation)));

    }

    /**
     * Builds the string by to be cached
     * @param a operant
     * @param b operant
     * @param request requst
     * @param result result of the operation
     * @param operation operator
     * @return string to be cached
     */
    private String getCacheString(String a, String b, HttpServletRequest request, String result, CalculatorOperations operation) {
        return request.getRemoteAddr() + " parameters " + a + " " + operation.getDisplayName() + " " + b + " result " + result;
    }

    /**
     * utility print entries to PrintStream
     */
    public void printEntries() {
        Cache cache = CacheManager.getInstance().getCache(PATH);
        for (Object key : cache.getKeys()) {
            Element element = cache.get(key);
            if (element != null && !element.isExpired()) {
                System.out.println(element.getObjectKey() + ": " + element.getObjectValue());
            }

        }
    }

    /**
     * Serves to get the list of entries
     * @return Arraylist with the entries
     */
    public ArrayList getEntries() {
        ArrayList<Record> records = new ArrayList<Record>();
        Cache cache = CacheManager.getInstance().getCache(PATH);
        for (Object key : cache.getKeys()) {
            Element element = cache.get(key);
            if (element != null && !element.isExpired()) {
                Record r = new Record();
                r.setId(element.getObjectKey().toString());
                r.setRecord(element.getObjectValue().toString());
                records.add(r);
            }
        }
        return records;
    }
}
