package net.adamslo.monetukol.api;

/**
 * supported operations with labels
 */
public enum CalculatorOperations {
    PLUS("Plus"), MULTIPLY("multiple by"), MAGIC("(Magic)");
    /**
     * display name holder
     */
    private String displayName;

    CalculatorOperations(String displayName) {
        this.displayName = displayName;
    }

    /**
     * Human readable operation
     * @return string with human readable content
     */
    public String getDisplayName() {
        return displayName;
    }
}
