package net.adamslo.monetukol.Calculator;


import net.adamslo.monetukol.api.CalculatorOperations;

import java.text.ParseException;

public class Calculator {
    /**
     * String representation for not a number format
     */
    public static final String NAN = "NaN";

    public Number calculate(CalculatorOperations operation, Integer... args) throws CalculatorException, ParseException {
        String[] argsstring = new String[args.length];
        int i = 0;
        for (Integer j : args) {
            argsstring[i] = j.toString();
            i++;
        }
        return calculate(operation, argsstring);
    }

    /**
     * @param operation the operation to be triggeres using args
     * @param args      generic arguments for operation
     * @return result od operation
     * @throws CalculatorException - when unsupported operator or invalid parameters (count...)
     */
    public Number calculate(CalculatorOperations operation, String... args) throws CalculatorException, ParseException {
        Number result = null;
        switch (operation) {
            case MULTIPLY:
                if (args.length == 2)
                    result = multiply(getNumber(args[0]), getNumber(args[1]));
                break;
            case PLUS:
                if (args.length == 2)
                    result = plus(getNumber(args[0]), getNumber(args[1]));
                break;
            case MAGIC:
                if (args.length == 2)
                    result = magic(args[0], args[1]);
                break;
            default:
                throw new CalculatorException("Invalid ooperation");
        }
        if (result == null) {
            throw new CalculatorException("Another error");
        }
        return result;
    }

    private Number magic(String a, String b) throws CalculatorException {
        return Math.pow(2, Math.random());
    }

    /**
     * generic plus over Numebr
     *
     * @param a - operand a
     * @param b - operand b
     * @return - a+b by type
     */
    private Number plus(Number a, Number b) {

        if (a instanceof Float && b instanceof Float) {
            return a.floatValue() + b.floatValue();
        } else if (a instanceof Long && b instanceof Long) {
            return a.longValue() + b.longValue();
        } else if (a instanceof Integer && b instanceof Integer) {
            return a.intValue() + b.intValue();
        } else {
            return a.doubleValue() + b.doubleValue();
        }
    }

    /**
     * generic multiply over Numebr
     *
     * @param a - operand a
     * @param b - operand b
     * @return - a*b by type
     */
    private Number multiply(Number a, Number b) {
        if (a instanceof Integer && b instanceof Integer) {
            return a.intValue() * b.intValue();
        } else if (a instanceof Float && b instanceof Float) {
            return a.floatValue() * b.floatValue();
        } else if (a instanceof Long && b instanceof Long) {
            return a.longValue() * b.longValue();
        } else {
            return a.doubleValue() * b.doubleValue();
        }
    }


    /**
     * Normalize string input into the Number. only illustrates convertion to propper type
     *
     * @param a some string to be intepret as Number
     * @return number Instance
     */
    private Number getNumber(String a) throws NumberFormatException, ParseException {
        Number result;
        if (a.contains("e")) {
            result = new Float(a);
        } else if (a.contains(".")) {
            result = new Double(a);
        } else {
            result = new Integer(a);
        }
        return result;
    }


}

