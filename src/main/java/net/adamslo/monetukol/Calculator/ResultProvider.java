package net.adamslo.monetukol.Calculator;

import net.adamslo.monetukol.Cache.EhCache;
import net.adamslo.monetukol.api.CalculatorOperations;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.util.concurrent.atomic.AtomicLong;


public class ResultProvider {

    /**
     * counter
     */
    private static final AtomicLong counter = new AtomicLong();

    /**
     * serves to compute result as required
     * @param a operant
     * @param b operant
     * @param request the request
     * @param operation oprator
     * @return Result of computition
     */
    public Result getResult(String a, String b, HttpServletRequest request, CalculatorOperations operation) {
        Calculator calculator = new Calculator();
        String resultString = "";
        try {
            Number result = calculator.calculate(operation, a, b);
            resultString = result.toString();
        } catch (NumberFormatException | ParseException | CalculatorException e) {
            resultString = Calculator.NAN;
        }

        Long l = counter.incrementAndGet();
        EhCache ec = new EhCache();
        ec.write(a, b, request, resultString, l, operation);
        return new Result(l, resultString);
    }
}
