package net.adamslo.monetukol.Calculator;


/**
 * generic result
 */
public class Result {

    private final long id;
    private final String result;

    /**
     * @param id     - the result id
     * @param result - result as a string
     */
    public Result(long id, String result) {
        this.id = id;
        this.result = result;
    }

    /**
     * rquset id result
     *
     * @return id of result
     */
    public long getId() {
        return id;
    }

    /**
     * @return generic content
     */
    public String getContent() {
        return result;
    }

}