package net.adamslo.monetukol.Controllers;

import net.adamslo.monetukol.Calculator.Result;
import net.adamslo.monetukol.Calculator.ResultProvider;
import net.adamslo.monetukol.api.CalculatorOperations;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;


@RestController
public class CalculatorController {

    /**
     * @param a       operant
     * @param b       operant
     * @param request request
     * @return Result as Number
     */
    @RequestMapping(method = RequestMethod.POST, value = "plus/{a}/{b}")
    public Result add(@PathVariable String a, @PathVariable String b, HttpServletRequest request) {
        ResultProvider rp = new ResultProvider();
        return rp.getResult(a, b, request, CalculatorOperations.PLUS);
    }

    /**
     * @param a       operant
     * @param b       operant
     * @param request request
     * @return Result as Number
     */
    @RequestMapping(method = RequestMethod.POST, value = "multiply/{a}/{b}")
    public Result multiply(@PathVariable String a, @PathVariable String b, HttpServletRequest request) {
        ResultProvider rp = new ResultProvider();
        return rp.getResult(a, b, request, CalculatorOperations.MULTIPLY);
    }

    /**
     * @param a       operant
     * @param b       operant
     * @param request request
     * @return Result as Number
     */
    @RequestMapping(method = RequestMethod.POST, value = "magic/{a}/{b}")
    public Result magic(@PathVariable String a, @PathVariable String b, HttpServletRequest request) {
        ResultProvider rp = new ResultProvider();
        return rp.getResult(a, b, request, CalculatorOperations.MAGIC);
    }

}
