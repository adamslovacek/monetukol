package net.adamslo.monetukol.Controllers;

import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * error page handler only
 */
@RestController
public class ErrorControllers implements ErrorController {

    private static final String PATH = "/error";

    @RequestMapping(value = PATH)
    public String error() {
        return "Something wrong? Tell Adam.";
    }

    @Override
    public String getErrorPath() {
        return PATH;
    }
}