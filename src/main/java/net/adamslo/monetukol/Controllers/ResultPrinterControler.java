package net.adamslo.monetukol.Controllers;

import net.adamslo.monetukol.Calculator.Calculator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ResultPrinterControler {
    private final int RANDEXP = 1000;

    @RequestMapping("/calculator")
    public String calculator(Model model) {
        Calculator calculator = new Calculator();
        int a = (int) (Math.random() * RANDEXP);
        int b = (int) (Math.random() * RANDEXP);

        model.addAttribute("a", a);
        model.addAttribute("b", b);

        return "calculator";
    }

}