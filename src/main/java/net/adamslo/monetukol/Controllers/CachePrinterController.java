package net.adamslo.monetukol.Controllers;

import net.adamslo.monetukol.Cache.EhCache;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
public class CachePrinterController {
    /**
     * @param model model to modify
     * @return the cache template
     */
    @RequestMapping("/cache")
    public String cache(Model model) {
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
        model.addAttribute("datetime", timeFormat.format(new Date()));
        EhCache ec = new EhCache();
        model.addAttribute("data", ec.getEntries());
        return "cache";
    }
}