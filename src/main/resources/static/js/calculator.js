$(".resulttrigger").keyup(function () {
    getResult(this);
});

$("select.resulttrigger").change(function () {
    getResult(this);
});

$(".resulttrigger").closest('form').submit(function(){
    getResult(this);
    return false;
});

function getResult(formElement){
    var form=$(formElement).closest('form');
    var operation=$(form).find('#operator').val();
    var a=$(form).find('input[name=a]').val();
    var b=$(form).find('input[name=b]').val();
    if(a.length>0 && b.length>0){
        $.post( "/"+operation+"/"+a+"/"+b+"/", function( data ) {
            $('.result').html(data.content);
        });
    }

}