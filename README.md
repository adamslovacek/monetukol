Zadani ukolu:
Úkolem je vytvořit webovou aplikaci, která sčítá zadaná čísla.
Webová aplikace běží na localhostu.

Aplikace se skládá:
1. z backend části (endpoint addition), napsané v Java pomocí Spring (např, Spring Boot)
Služby jsou vystavené pomocí REST rozhraní
https://spring.io/guides/gs/rest-service/

2. z frontend části (HTML5, Javascript, CSS3)
Pro FE může být použitý framework, např. AngularJS nebo Bootstrap.

Příklad, jak může vypadat FE:
(nakresleny formular)

3. Další funkcionalita
Je použit Spring Boot.
Každé volání na endpoint addition se zapíše do cache (access log) záznam s informací o IP adrese, parametrech a výsledku v rámci serveru(cache má platnost záznamu 5 minut).
pro uložení záznamů je použita nějaká implementace cache - např. ehcache.

Aplikace je dále rozšířená:
o backend (endpoint log) z backend části (Spring MVC), která zobrazuje data z access logu.
Stránka se automaticky obnovuje po půl minutě. 

Příklad, jak může vypadat FE:
* Shows IP address, parameters, result:
* 195.134.123 parameters 2, 3 result 5
* 195.134.123 parameters 1, 3 result 4
* 195.134.123 parameters 1, dddd result error
* 195.134.111 parameters 1, 1 result 2